import React, { Component, PureComponent } from "react";

// PureComponent: hạn chế những render không cần thiết trong quá trình update ~ primitive ~ pass by value ~ shallow compare
export default class Header extends PureComponent {
  //   componentDidMount() {
  //     this.myCountDown = setInterval(() => {
  //       console.log("count down");
  //     }, 1000);
  //   }
  //   shouldComponentUpdate(nextProps, nextState) {
  //     console.log(nextProps);
  //     if (nextProps.like == 3) {
  //       return false;
  //     }
  //     return true;
  //   }
  render() {
    console.log("HEADER render");
    return (
      <div className="bg-primary display-2 text-white">
        <span>Header</span>

        <p> Like: {this.props.like}</p>
      </div>
    );
  }
  componentWillUnmount() {
    // Tự động được chạy khi component bị remove khỏi giao diện
    console.log("componentWillUnmount");
    clearInterval(this.myCountDown);
  }
}
