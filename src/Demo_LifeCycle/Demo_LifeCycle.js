import { Button } from "antd";
import React, { Component } from "react";
import Header from "./Header";

export default class Demo_LifeCycle extends Component {
  componentDidMount() {
    // Chỉ chạy 1 lần duy nhất sau khi giao diện render lần đầu tiên
    console.log("componentDidMount");
  }
  state = {
    like: 1,
    share: 1,
  };
  componentDidUpdate() {
    // tự động được chạy sau khi render() chay
    console.log("componentDidUpdate");
  }
  render() {
    console.log("render");
    return (
      <div>
        {this.state.like < 5 && <Header like={this.state.like} />}
        <div className="pt-5">
          <span className="display-4 mx-5">{this.state.like}</span>

          {/* <button
            onClick={() => {
              this.setState({ like: this.state.like + 1 });
            }}
            className="btn btn-success"
          >
            Plus like
          </button> */}
          <Button type="primary">Plus like</Button>
        </div>
        <div className="pt-5">
          <span className="display-4 mx-5">{this.state.share}</span>

          <button
            onClick={() => {
              this.setState({ share: this.state.share + 1 });
            }}
            className="btn btn-danger"
          >
            Plus share
          </button>
        </div>
      </div>
    );
  }
}
