import React, { Component } from "react";
import { phoneArr } from "./data_phone";
import PhoneDetail from "./PhoneDetail";
import PhoneList from "./PhoneList";

export default class Ex_Phones extends Component {
  state = {
    phoneArr: phoneArr,
    phoneDetail: phoneArr[0],
  };
  handleShowDetail = (item) => {
    this.setState({ phoneDetail: item });
  };
  render() {
    return (
      <div className="container py-5">
        <PhoneList
          handleShowDetail={this.handleShowDetail}
          data={this.state.phoneArr}
        />
        <PhoneDetail phoneDetail={this.state.phoneDetail} />
      </div>
    );
  }
}
