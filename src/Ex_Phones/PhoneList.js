import React, { Component } from "react";
import shortid from "shortid";
import PhoneItem from "./PhoneItem";

export default class PhoneList extends Component {
  renderPhoneList = () => {
    return this.props.data.map((item) => {
      let randomNum = shortid.generate();
      // console.log("randomNum: ", randomNum);
      return (
        <PhoneItem
          handleShowDetail={this.props.handleShowDetail}
          data={item}
          key={randomNum}
        />
      );
    });
  };

  render() {
    // console.log(this.props.data);
    return <div className="row">{this.renderPhoneList()}</div>;
  }
}
