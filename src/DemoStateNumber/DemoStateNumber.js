import React, { Component } from "react";

export default class DemoStateNumber extends Component {
  state = {
    number: 1,
  };
  handlePlusNumber = () => {
    this.setState({ number: this.state.number + 1 });
  };
  handleDecNumber = () => {
    this.setState({ number: this.state.number - 1 });
  };
  render() {
    return (
      <div>
        <button onClick={this.handleDecNumber}>Dec Number</button>
        <span className="display-4 mx-5">{this.state.number}</span>
        <button onClick={this.handlePlusNumber}>Plus Number</button>
      </div>
    );
  }
}
