import React, { Component } from "react";

export default class SanPhamChiTiet extends Component {
  render() {
    console.log("this.props", this.props);
    return (
      <div>
        <p>Tên sản phẩm: {this.props.detail.name}</p>
        <p>Giá sản phẩm: {this.props.detail.price}</p>
        <p>User: {this.props.username}</p>
      </div>
    );
  }
}
