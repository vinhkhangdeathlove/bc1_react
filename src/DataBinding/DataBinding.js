// import React, { Component } from "react";

// export default class DataBinding extends Component {
//   render() {
//     let tenSanPham = "Iphone 13";
//     let linkSanPham =
//       "https://tse2.mm.bing.net/th?id=OIP.omBsuDDGNC5dtRchvfvN4gHaEx&pid=Api&P=0&w=275&h=177";
//     let renderGiaSanpham = () => {
//       return "15.000";
//     };
//     return (
//       <div>
//         <div className="card" style={{ width: "18rem" }}>
//           <img src={linkSanPham} className="card-img-top" alt="..." />
//           <div className="card-body">
//             <h5
//               style={{ color: "red", backgroundColor: "black" }}
//               className="card-title"
//             >
//               {tenSanPham}
//             </h5>
//             <p className="card-text">
//               Some quick example text to build on the card title and make up the
//               bulk of the card's content.
//             </p>
//             <a href="#" className="btn btn-primary">
//               {renderGiaSanpham()}
//             </a>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

import React, { Component } from "react";

export default class DataBinding extends Component {
  render() {
    let tenSanPham = "Iphone 13";
    let linkSanPham =
      "https://cdn11.bigcommerce.com/s-p8i5esyy64/images/stencil/960w/products/1952/7979/iphone-13-blue-select-2021__08080.1635344242.png?c=2";
    let renderGiaSanpham = () => {
      return "15.000";
    };
    return (
      <div>
        <div className="card" style={{ width: "18rem" }}>
          <img src={linkSanPham} className="card-img-top" alt="" />
          <div className="card-body">
            <h5
              className="card-title"
              style={{ color: "red", backgroundColor: "blue" }}
            >
              {tenSanPham}
            </h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              {renderGiaSanpham()}
            </a>
          </div>
        </div>
      </div>
    );
  }
}
