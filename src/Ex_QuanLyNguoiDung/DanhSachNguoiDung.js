import React, { Component } from "react";

export default class DanhSachNguoiDung extends Component {
  renderDanhSachNguoiDung = () => {
    return this.props.danhSachNguoiDung.map((item, index) => {
      let { hoTen, taiKhoan, matKhau, email, id } = item;
      return (
        <tr key={index}>
          <td>{id}</td>
          <td>{hoTen}</td>
          <td>{taiKhoan}</td>
          <td>{matKhau}</td>
          <td>{email}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemoveUser(id);
              }}
              className="btn btn-danger"
            >
              Xoá
            </button>
            <button
              onClick={() => {
                this.props.handleEditUser(id);
              }}
              className="btn btn-warning"
            >
              Sửa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table mt-5">
          <thead>
            <th>ID</th>
            <th>Tên</th>
            <th>Tài khoản</th>
            <th>Mật khẩu</th>
            <th>Email</th>
            <th>Thao tác</th>
          </thead>
          <tbody>
            {/* render danh sach người dùng  */}
            {this.renderDanhSachNguoiDung()}
          </tbody>
        </table>
      </div>
    );
  }
}
