import { GIAM_SO_LUONG, TANG_SO_LUONG } from "../constants/numberConstant";

export const tangSoLuongAction = (value) => {
  return {
    type: TANG_SO_LUONG,
    payload: value,
  };
};
