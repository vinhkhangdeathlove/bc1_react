import React, { Component } from "react";
import { connect } from "react-redux";
import { tangSoLuongAction } from "./redux/action/numberAction";
import { GIAM_SO_LUONG, TANG_SO_LUONG } from "./redux/constants/numberConstant";

class DemoReduxMini extends Component {
  render() {
    return (
      <div>
        <button
          onClick={() => {
            this.props.tangSoLuong(5);
          }}
          className="btn btn-danger"
        >
          Increase
        </button>
        <span className="text-success display-4">{this.props.number}</span>
        <button
          onClick={() => {
            this.props.giamSoLuong(5);
          }}
          className="btn btn-danger"
        >
          Decrease
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    number: state.number.giaTri,
  };
  /**
   * key: tên props mà component hiện tại sử dụng
   * value: giá trị trên store
   */
};

let mapDispatchToProps = (dispatch) => {
  return {
    tangSoLuong: (value) => {
      // action: mô tả thông tin cho reducer xử lý
      // dispatch: callback redux sẽ gửi vào, nhận action làm tham số
      dispatch(tangSoLuongAction(value));
    },
    giamSoLuong: (value) => {
      let action = {
        type: GIAM_SO_LUONG,
        payload: value,
      };
      // action: mô tả thông tin chỏ reducer xử lý
      // dispatch: callback redux sẽ gửi vào, nhận action làm tham số
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoReduxMini);
