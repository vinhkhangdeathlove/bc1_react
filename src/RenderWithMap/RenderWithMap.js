import React, { Component } from "react";
import { movieArrr } from "./data_renderWithMap";
import ItemMovie from "./ItemMovie";

export default class RenderWithMap extends Component {
  state = {
    listMovie: movieArrr,
  };
  renderListMovie = () => {
    let listItem = this.state.listMovie.map((item) => {
      return (
        <div className="col-4">
          <ItemMovie movie={item} key={item.maPhim} />
        </div>
      );
    });
    return listItem;
  };
  render() {
    return (
      <div className="container py-5">
        <div className="row">{this.renderListMovie()}</div>
      </div>
    );
  }
}
