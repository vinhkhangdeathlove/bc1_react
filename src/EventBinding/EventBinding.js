import React, { Component } from "react";

export default class EventBinding extends Component {
  isLogin = true;
  hangdleLogin = () => {
    console.log("yes");
  };
  hangdleLoginWithParams = (username) => {
    console.log("Bye" + username);
  };
  renderContent = () => {
    if (this.isLogin) {
      return (
        <>
          <button onClick={this.hangdleLogin} className="btn btn-success">
            Logout
          </button>
          <button
            onClick={() => {
              this.hangdleLoginWithParams(" Alice");
            }}
            className="btn btn-success"
          >
            Logout with User
          </button>
        </>
      );
    } else return <button className="btn btn-warning">Login</button>;
  };
  render() {
    return (
      <div className="container py-5">
        <div>{this.renderContent()}</div>
        <div>{this.isLogin ? "Đã đăng nhập" : "Chưa đăng nhập"}</div>
      </div>
    );
  }
}
