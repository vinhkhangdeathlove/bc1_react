import React, { Component } from "react";
import { connect } from "react-redux";
import { LUA_CHON } from "./redux/constant/xucXacConstant";
import { Tai, Xiu } from "./utils/xucXacUtils";

// let styles = {
//   btnGame: {
//     fontSize: "40px",
//     width: "100px",
//     height: "100px",
//   },
// };

class xucXac extends Component {
  render() {
    console.log(this.props);
    return (
      <div className="d-flex justify-content-between align-items-center">
        {/* <button
          onClick={() => {
            this.props.handleChoseOption(Tai);
          }}
          style={styles.btnGame}
          className="text-white btn-danger"
        >
          Tài
        </button> */}
        <div>
          {this.props.mangXucXac.map((item) => {
            return (
              <img
                style={{ width: 75, margin: "0 10px" }}
                src={item.img}
                alt=""
              />
            );
          })}
        </div>
        {/* <button
          onClick={() => {
            this.props.handleChoseOption(Xiu);
          }}
          style={styles.btnGame}
          className="text-white btn-secondary"
        >
          Xỉu
        </button> */}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    mangXucXac: state.xucXacReducer.mangXucXac,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChoseOption: (luaChon) => {
      dispatch({
        type: LUA_CHON,
        payload: luaChon,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(xucXac);
