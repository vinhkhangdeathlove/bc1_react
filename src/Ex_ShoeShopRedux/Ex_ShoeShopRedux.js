import React, { Component } from "react";
import { shoeArr } from "./data_shoeShop";
import ItemShoe from "./ItemShoe";
import ListShoe from "./ListShoe";
import TableGioHang from "./TableGioHang";

export default class Ex_ShoeShopRedux extends Component {
  handleRemoveShoe = (idShoe) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idShoe;
    });
    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };

  handleChangeQuantity = (idShoe, step) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idShoe;
    });
    let cloneGioiHang = [...this.state.gioHang];

    cloneGioiHang[index].soLuong += step;
    if (cloneGioiHang[index].soLuong === 0) {
      cloneGioiHang.splice(index, 1);
    }

    this.setState({ gioHang: cloneGioiHang }, () => {
      console.log(this.state.gioHang.length);
    });
  };
  render() {
    return (
      <div className="container py-5">
        {/* {this.state.gioHang.length > 0 && ( */}
        <TableGioHang
          handleRemoveShoe={this.handleRemoveShoe}
          // gioHang={this.state.gioHang}
          handleChangeQuantity={this.handleChangeQuantity}
        />
        {/* )} */}

        {/* <div className="row">{this.renderShoes()}</div> */}
        <ListShoe />
      </div>
    );
  }
}
