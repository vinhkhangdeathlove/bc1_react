import { combineReducers } from "redux";
import { shoeShopReducer } from "./shoeShopReducer";

export let rootReducer_shoeShop = combineReducers({
  shoeShopReducer, //key value giống nhau, ES6 cho rút gọn {shoeShopReducer: shoeShopReducer}
});
