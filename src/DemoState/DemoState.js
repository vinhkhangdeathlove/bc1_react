import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    isLogin: true,
  };
  handleLogin = () => {
    console.log("Login");
    this.setState({ isLogin: true });
  };
  handleLogout = () => {
    console.log("Logout");
    this.setState({ isLogin: false });
  };
  render() {
    return (
      <div>
        <p>
          {this.state.isLogin ? (
            <div>
              <p>Đã đăng nhập</p>
              <button onClick={this.handleLogout} className="btn btn-danger">
                Logout
              </button>
            </div>
          ) : (
            <div>
              <p>Chưa đăng nhập</p>
              <button onClick={this.handleLogin} className="btn btn-success">
                Login
              </button>
            </div>
          )}
        </p>
      </div>
    );
  }
}
